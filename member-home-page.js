import HomePage from './/home-page'
import {userData} from '../test-data/user'

class MemberHome{

    get AccountSummaryTab()                 { return $("//a[contains(text(),'Account Summary')]");}

    get AccountActivityTab()                { return $("//a[contains(text(),'Account Activity')]");}

    get OnlineBankingText()                 { return $("//div[@class='span3']//h4[contains(text(),'Online Banking')]");}
    get ServicesButton()                    { return $("//a[@id='online-banking']");}
    get OnlineBankingBannerText()           { return $("//h1[contains(text(),'Online Banking')]");}
    
    get AccountSummaryButton()              { return $("//span[@id='account_summary_link']");}
    get TransferFundsTab()                  { return $("//a[contains(text(),'Transfer Funds')]");}
    get TransferFundsText()                 { return $('//*[@id="transfer_funds_content"]/form/div/div/h2');}
    get FromAccountDropDown()               { return $("//select[@id='tf_fromAccountId']")};
    get SelectedFromAccountDropDown()       { return $("//select[@id='tf_fromAccountId']//option[@value='3'][contains(text(),'Savings(Avail. balance = $ 1548)')]")};
    get ToAccountDropDown()                 { return $("//select[@id='tf_toAccountId']")};
    get SelectedToAccountDropDown()         { return $("//select[@id='tf_toAccountId']//option[@value='3'][contains(text(),'Savings(Avail. balance = $ 1548)')]")};
    get AmmountInput()                      { return $("//input[@id='tf_amount']")};
    get DescriptionInput()                  { return $("//input[@id='tf_description']")};
    get ContinueButton()                    { return $("//button[@id='btn_submit']")};
    get TransferVerifyText()                { return $('//*[@id="transfer_funds_content"]/form/div/div/h2');}
    get TransferSubmitButton()              { return $("//button[@class='btn btn-primary']");}
    get TransferSuccessText()               { return $("//div[@class='alert alert-success']");}

    
    get PayBillsButton()                    { return $("//span[@id='pay_bills_link']");}
    get PayBillsTab()                       { return $("//a[contains(text(),'Pay Bills')]");}

    get PayeeDropDown()                     { return $("//select[@id='sp_payee']");}
    get SelectedPayeeDropDown()             { return $("//option[@value='apple']")};
    get AccountDropDown()                   { return $("//select[@id='sp_account']");}
    get SelectedAccountDropDown()           { return $("//option[@value='3']");}
    get PaymentAmount()                     { return $("//input[@id='sp_amount']");}
    get DateInput()                         { return $("//input[@id='sp_date']");}
    get CalendarInput()                     { return $('//*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[4]/a');}
    get PaymentDescription()                { return $("//input[@id='sp_description']");}
    get PaymentButton()                     { return $("//input[@id='pay_saved_payees']");}
    get PaymentSuccessText()                { return $("//span[contains(text(),'The payment was successfully submitted')]");}

    get AddPayeeTab()                       { return $("//a[contains(text(),'Add New Payee')]");}
    get AddPayeeHeaderText()                { return $("//h2[contains(text(),'Who are you paying?')]");}
    get PayeeNameInput()                    { return $("//input[@id='np_new_payee_name']");}
    get PayeeAddressInput()                 { return $("//textarea[@id='np_new_payee_address']");}
    get PayeeAccountNameInput()             { return $("//input[@id='np_new_payee_account']");}
    get PayeeAccountDetailsInput()          { return $("//input[@id='np_new_payee_details']");}
    get PayeeAddButton()                    { return $("//input[@id='add_new_payee']");}
    get PayeeAddSuccessText()               { return $("//div[contains(text(),'The new payee "+ userData.payeeData.name + " was successfully created')]" );}

    get PurchaseForeignCurrencyTab()        { return $("//a[contains(text(),'Purchase Foreign Currency')]");}
    get PurchaseForeignCurrencyHeaderText() { return $("//h2[contains(text(),'Purchase foreign currency cash')]");}
    get CurrencyDropDown()                  { return $("//select[@id='pc_currency']");}
    get SelectedCurrencyDropDown()          { return $("//option[@value='EUR']");}
    get CurrencyAmountInput()               { return $("//input[@id='pc_amount']");}
    get CurrencyUSARadioButton()            { return $("//input[@id='pc_inDollars_true']");}
    get CalculateCostButton()               { return $("//input[@id='pc_calculate_costs']");}
    get CurrencyConversionAmmount()         { return $("//label[@id='pc_conversion_amount']");}
    get CurrencyPurchaseButton()            { return $("//input[@id='purchase_cash']");}
    get CurrencyPurchaseSuccessText()       { return $("//div[contains(text(),'Foreign currency cash was successfully purchased')]");}


    get MyMoneyMapTab()                     { return $("//a[contains(text(),'My Money Tab')]");}
    get OnlineStateMentsTab()               { return $("//a[contains(text(),'Online Statements')]");}


    userLogin(){
        new HomePage()
            .clickUserLoginLink()
            .addValueUsernameText(userData.defaultUser.username)
            .addValuePasswordText(userData.defaultUser.password)
            .clickUserLoginButton();
    }

    transferFunds(){
        this.OnlineBankingText.waitForDisplayed(10000);
        this.ServicesButton.click();
        this.OnlineBankingBannerText.waitForDisplayed(10000);
        this.AccountSummaryButton.click();
        this.AccountSummaryTab.waitForDisplayed(10000);
        this.TransferFundsTab.click();
        this.TransferFundsText.waitForDisplayed(10000);
        this.FromAccountDropDown.click();
        this.SelectedFromAccountDropDown.click();
        this.ToAccountDropDown.click();
        this.SelectedToAccountDropDown.click();
        this.AmmountInput.setValue(userData.transferData1.amount);
        this.DescriptionInput.setValue(userData.transferData1.description);
        this.ContinueButton.click();
        this.TransferVerifyText.waitForDisplayed(10000);
        this.TransferSubmitButton.click();
        this.TransferSuccessText.waitForDisplayed(10000);

    }

    makePayment(){
        this.OnlineBankingText.waitForDisplayed(10000);
        this.ServicesButton.click();
        this.OnlineBankingBannerText.waitForDisplayed(10000);
        this.PayBillsButton.click();
        this.PayBillsTab.waitForDisplayed(10000);
        this.PayeeDropDown.click();
        this.SelectedPayeeDropDown.click();
        this.AccountDropDown.click();
        this.SelectedPayeeDropDown.click();
        this.PaymentAmount.setValue(userData.paymentData.amount);
        this.DateInput.click();
        this.CalendarInput.click();
        this.PaymentDescription.setValue(userData.paymentData.description);
        this.PaymentButton.click();
        this.PaymentSuccessText.waitForDisplayed(10000);
        
    }

    addNewPayee(){
        this.OnlineBankingText.waitForDisplayed(10000);
        this.ServicesButton.click();
        this.OnlineBankingBannerText.waitForDisplayed(10000);
        this.PayBillsButton.click();
        this.PayBillsTab.waitForDisplayed(10000);
        this.AddPayeeTab.click();
        this.AddPayeeHeaderText.waitForDisplayed(10000);
        this.PayeeNameInput.setValue(userData.payeeData.name);
        this.PayeeAddressInput.setValue(userData.payeeData.address);
        this.PayeeAccountNameInput.setValue(userData.payeeData.account);
        this.PayeeAccountDetailsInput.setValue(userData.payeeData.details);
        this.PayeeAddButton.click();
        this.PayeeAddSuccessText.waitForDisplayed(10000);

    }

    purchaseForeignCurrency(){
        this.OnlineBankingText.waitForDisplayed(10000);
        this.ServicesButton.click();
        this.OnlineBankingBannerText.waitForDisplayed(10000);
        this.PayBillsButton.click();
        this.PayBillsTab.waitForDisplayed(10000);
        this.PurchaseForeignCurrencyTab.click();
        this.PurchaseForeignCurrencyHeaderText.waitForDisplayed(1000);
        this.CurrencyDropDown.click();
        this.SelectedCurrencyDropDown.click();
        this.CurrencyAmountInput.setValue(userData.purchaseCurrencyData.amount);
        this.CurrencyUSARadioButton.click();
        this.CalculateCostButton.click();
        this.CurrencyConversionAmmount.waitForDisplayed(10000);
        this.CurrencyPurchaseButton.click();
        this.CurrencyPurchaseSuccessText.waitForDisplayed(10000);
    }
}

export default MemberHome;